#!/bin/sh

GENOME=C_sativa_DH55_Apr_2014
FLANK=5 
QUICKLOAD=/lustre/groups/lorainelab/data/genomes/pub/quickload
FILES=$(ls ../*.bam)
TWOBIT=$QUICKLOAD/$GENOME/$GENOME.2bit
for F in $FILES
do
SAMPLE=$(basename $F .bam)
if [ ! -f $SAMPLE.FJ.bed.gz ]; then 
    qsub -o $SAMPLE.out -e $SAMPLE.err -N $SAMPLE.NFJ -v FLANK=$FLANK,FILE=$F,SAMPLE=$SAMPLE,TWOBIT=$TWOBIT nFJ_cluster.sh
fi
done
