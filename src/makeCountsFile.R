# convert output of featureCounts into a table of
# counts per gene per sample, for 2017 Workshop

source("../src/ProjectWide.R")
filename='../Counts/data/counts_sm.txt.gz'
d=read.delim(filename,header=TRUE,
             sep="\t",comment.char = '#',
             as.is=TRUE)
row.names(d)=d$Geneid
indexes=grep(x=names(d),pattern='SRR')
d=d[,indexes]
newnames=unlist(strsplit(x=names(d),split='.bam'))
names(d)=newnames
sra2sample=getSraToSampleName()
v=sra2sample$sample
names(v)=sra2sample$id
names(d)=v[names(d)]
samples=names(d)
d$gene=row.names(d)
d=d[,c('gene',samples)]
fname='../Workshop2017/counts.txt'
write.table(d,file=fname,sep='\t',row.names = F,quote=F)





