#!/bin/bash
FILES=$(ls /lustre/groups/lorainelab/data/camelina/alignment_Processed/*.bam)
for FILE in $FILES
do
    echo $FILE
    ln -s $FILE .
done
FILES=$(ls /lustre/groups/lorainelab/data/camelina2/alignment_Processed/*.bam)
for FILE in $FILES
do
    echo $FILE
    ln -s $FILE .
done