#!/bin/bash
#PBS -l nodes=1:ppn=4
#PBS -l vmem=32000mb
#PBS -l walltime=24:00:00
cd $PBS_O_WORKDIR

module load tophat
module load samtools

# S,O,G defined by qsub -v 
tophat -p 4 --library-type fr-firststrand -I 5000 -i 30 -o $O/$S $G ${S}_1.fastq.gz ${S}_2.fastq.gz
