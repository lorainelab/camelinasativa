#!/bin/bash
# featureCounts v1.4.5
# /lustre/groups/lorainelab/sw/subread-1.4.5-Linux-x86_64/bin/featureCounts
# https://bitbucket.org/lorainelab/camelina
# -p data are paired-end
# -C do not count alignments if read pair members are on different chromosomes
# -O count each read for each feaure it overlaps, even if the features
# overlap other features 
SAF=$HOME/src/camelina/GeneRegions/results/SAF.txt
BASE=counts_mm
OUT=$BASE.tsv
ERR=$BASE.err
OUT2=$BASE.out
# include multi-mapping reads
featureCounts -p -O -C -M -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
BASE=counts_sm
OUT=$BASE.tsv
ERR=$BASE.err
OUT2=$BASE.out
# don't include multi-mapping reads
featureCounts -p -O -C -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2
BASE=counts_pm
OUT=$BASE.tsv
ERR=$BASE.err
OUT2=$BASE.out
# use primary alignment only
featureCounts -p -O -C --primary -F SAF -a $SAF -o $OUT *.bam 2>$ERR 1>$OUT2


