#!/bin/bash

# Run this script from the FastQC directory of one of the studies.

SRC="."
FILES=$(ls ../fastq/*.fastq*)

for f in $FILES
do
  export f
  sample=${f%.fastq*}
  sample=${sample#../fastq/}
  qsub -e logs/$sample.log -o logs/$sample.log -v f -N FastQC-$sample $SRC/runFastqc.pbs
done
