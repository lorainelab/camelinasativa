#!/bin/bash

# assumes you checked out camelina repository into directory named "src"
# in your HOME directory 
SRC=$HOME/src/camelina
FNAME=$SRC/ExternalDataSets/ForAnnotsXML.txt
SRRS=`cut -f 1 $FNAME`
FTP="ftp://ftp-trace.ncbi.nih.gov/sra/sra-instant/reads/ByRun/sra/SRR/SRR117"
for FILE in $SRRS;
do
    echo "checking $FILE"
    if [ ! -f $FILE.sra ]; then
	URL="$FTP/$FILE/$FILE.sra"
	echo "getting: $URL"
        wget $URL
    else
	echo "no need to get $FILE"
    fi
done