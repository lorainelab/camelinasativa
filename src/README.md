#########################################################################################
README: This README contains information about the Camelina src module in the git repository.

This src module contains the scripts used to generate data on the cluster. 
Each process has a run and call script.

Created by: Chris Ball

#########################################################################################

***
# callProcessAlignments.sh
Script used to call processAlignments.pbs. Takes the sample name from TopHat alignment directory.
Gets the the accepted_hits.bam file and the junctions.bed file from each sample directory. 
Sends the input files and sample names to processAlignment.pbs and creates a log file in the logs directory for each run.
Sends the process alignment command to qsub.

***
# callRunFastq_dump.sh
Script used to call runFastq_dump.pbs. Gets the sample name for each file downloaded from the SRA.
Sends each job to qsub and creates a log file in the logs directory for each run.
 
*** 
# callRunFastqc.sh
Script used to call runRunFastqc.pbs. Takes sample file name from the fastq files and passes it on.
Sends each job to qsub and creates a log file in the logs directory for each run.

***
# callRunFeatureCounts.sh
Script that calls runFeatureCounts.pbs for each process alignment in the alignment_Processed directory.
Passes the file name for each sample and creates a log file in the logs directory.
Sends each job to qsub.

***
# callRunTophatPaired.sh
Gets the sample names for all the Fastq files in the Fastq directory. 
Passes the name into runTophatPaired.pbs and creates a log file in the logs directory.
Sends each job to qsub.

***
# download_sra.sh
Script to download all the SRA file used in this project using wget with the NCBI FTP URL's.
Run this script in an interactive job.

***
# processAlignments.pbs
For each sample ID passed into processAlignments.pbs, perform a list of tasks.
Load needed modules.
Sort BAM file with samtools then use samtools to create BAM index files.
Use bedtools to make coverage graphs. 
Use tabix to create coverage index files.
Sort and compress the junction file.
Record the version number of the software used in the log file.

***
# rerunSingleFile.sh
Used to run runFastq_dump.pbs on a single file in place of all the files in the directory.

***
# runBowtie.pbs
Take the genome file as input and run Bowtie2-build for each input file.

***
# runFastq_dump.pbs
Convert SRA files to Fastq format. Split paired files into two Fastq files.

***
# runFastqc.pbs
Run FastQC for each input file and save results in FastQC directory.

***
# runFeatureCounts.pbs
Run feature counts on all the input sample files using the SAF file as input. 

***
# runTophatPaired.pbs
Run Tophat for each set of paired reads using the Camelina genome. 

***
# License

Copyright (c) 2015 University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT