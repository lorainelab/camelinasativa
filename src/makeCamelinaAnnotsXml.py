#!/usr/bin/env python

# to run this, get git repository https://bitbucket.org/lorainelab/igbquickload
# put igbquickload root directory in your PYTHONPATH

from Quickload import *
import QuickloadUtils as utils
from AnnotsXmlForRNASeq import *

ANNOTSBACKGROUND='FFFFFF'

colors={'ESD':"0049F6",
        'EMD':"800080", 
        'LMD':"CC3300",
        'LSD':"800080",
        'GrS':"00FFFF",
        'Cot':"00CC66",
        'YLf':"009900",
        'SLf':"FF9900",
        'Stm':"004C00",
        'Roo':"006699",
        'Bud':"CC00CC",
        'Flw':"FF6600"}

deploy_dir="SRP038024"
genome="C_sativa_DH55_Apr_2014"
u = "%s/%s"%(genome,deploy_dir)

# copied from ../ExternalDataSets/ForAnnotsXML.txt
txt="""SRR1171932	Bud.1	Buds
SRR1171933	Bud.2	Buds
SRR1171948	Bud.3	Buds
SRR1171949	Flw.1	Flower
SRR1171950	Flw.2	Flower
SRR1171951	Flw.3	Flower
SRR1171954	ESD.1	Early Seed Development, 4-12 DAP
SRR1171955	ESD.2	Early Seed Development, 4-12 DAP
SRR1171956	ESD.3	Early Seed Development, 4-12 DAP
SRR1171957	EMD.1	Early Mid Seed Development, 18-22 DAP
SRR1171959	EMD.2	Early Mid Seed Development, 18-22 DAP
SRR1171970	EMD.3	Early Mid Seed Development, 18-22 DAP
SRR1171971	LMD.1	Late Mid Seed Development, 26-30 DAP
SRR1171972	LMD.2	Late Mid Seed Development, 26-30 DAP
SRR1171973	LMD.3	Late Mid Seed Development, 26-30 DAP
SRR1171974	LSD.1	Late Seed Development, 32-40 DAP
SRR1171975	LSD.2	Late Seed Development, 32-40 DAP
SRR1171977	LSD.3	Late Seed Development, 32-40 DAP
SRR1171888	GrS.1	Germinating seedling
SRR1171890	GrS.2	Germinating seedling
SRR1171891	GrS.3	Germinating seedling
SRR1171892	Cot.1	Cotyledons
SRR1171894	Cot.2	Cotyledons
SRR1171895	Cot.3	Cotyledons
SRR1171925	Stm.1	Stem
SRR1171926	Stm.2	Stem
SRR1171927	Stm.3	Stem
SRR1171897	YLf.1	Young leaf
SRR1171899	YLf.2	Young leaf
SRR1171901	YLf.3	Young leaf
SRR1171928	Roo.1	Root
SRR1171929	Roo.2	Root
SRR1171931	Roo.3	Root
SRR1171920	SLf.1	Senescing leaf
SRR1171922	SLf.2	Senescing leaf
SRR1171923	SLf.3	Senescing leaf"""

def makeLsts():
    lsts=[]
    lines=txt.split('\n')
    for line in lines:
        toks=line.rstrip().split('\t')
        color=getColor(toks[1])
        lst=[toks[0],"%s %s"%(toks[1],toks[2]),color,ANNOTSBACKGROUND,u]
        lsts.append(lst)
    return lsts

def getColor(sample_code):
    key=sample_code.split('.')[0]
    color=colors[key]
    return color

                   
    
def makeAnnotsFiles():
    files=[]
    quickload_file = AnnotationFile(path="C_sativa_DH55_Apr_2014.bed.gz",track_name="Gene Models",
                                    track_info_url=genome,show2tracks=True,foreground_color="000000",
                                    load_all=True,
                                    tool_tip="Models from Kagale, et al.")
    files.append(quickload_file)
    return files
                                    
def makeQuickloadFiles():
    quickload_files = makeAnnotsFiles()
    lsts=makeLsts()
    rnaseq_files = makeQuickloadFilesForRNASeq(lsts=lsts,folder="RNA-Seq/Kagale, 2014 SRP038024",
                    deploy_dir=deploy_dir)
    for file in rnaseq_files:
        quickload_files.append(file)
    return quickload_files

if __name__=='__main__':
    about="Make annots.xml text for Camelina sativa genome."
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files=quickload_files)
