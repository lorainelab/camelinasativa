#!/bin/bash

# Run this script from the FastQC directory of one of the studies.

SRC="."
FILES=$(ls ../sra/SRR1171975.sra)

for f in $FILES
do
  export f
  sample=${f%.sra}
  sample=${sample#../sra/}
  qsub -e logs/$sample.log -o logs/$sample.log -v f -N Fast_dump-$sample $SRC/runFastq_dump.pbs
done
