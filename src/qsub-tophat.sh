#!/bin/bash

# to run: 
#    qsub-tophat.sh >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 

SCRIPT=tophat.sh
Q=copperhead
G=C_sativa_DH55_Apr_2014
O=tophat

FS=$(ls *_1.fastq.gz)
for F in $FS
do
    S=$(basename $F _1.fastq.gz)
    if [ ! -f $O/$S/accepted_hits.bam ]; then
	CMD="qsub -q $Q -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G $SCRIPT"
	$CMD
    fi
done

