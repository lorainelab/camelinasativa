#!/bin/bash

# assumes runFastq_dump.pbs is in current working directory
# assumes sra files are in ../sra
# run this inside fastq directory, where files should be written

SRC="."
SRA="SRP038024"
FILES=$(ls ../../sra/$SRA/*.sra)

for f in $FILES
do
  export f
  sample=$(basename "$f")
  sample=${sample%.sra}
  qsub -e logs/$sample.log -o logs/$sample.log -v f -N $sample $SRC/runFastq_dump.pbs
done
