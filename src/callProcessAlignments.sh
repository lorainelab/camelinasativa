#!/bin/bash

# Run this script from the alignment_processed directory.

SRC="."
FILES=$(ls ../alignment_TopHat/logs)

for FILE in $FILES
do
  SAMPLE=${FILE%.log}
  # copy the bam file to this directory using the sample name
  cp ../alignment_TopHat/$SAMPLE/accepted_hits.bam ./$SAMPLE.bam
  # copy the junctions file to this directory
  cp ../alignment_TopHat/$SAMPLE/junctions.bed ./$SAMPLE.junctions.bed
  export SAMPLE
  qsub -e logs/$SAMPLE.log -o logs/$SAMPLE.log -v SAMPLE -N ind.$SAMPLE $SRC/processAlignments.pbs
done
