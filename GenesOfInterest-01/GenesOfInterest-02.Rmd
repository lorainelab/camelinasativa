---
title: "Selected genes' expression across all tissues"
author: "Ivory Clabaugh Blakley"
output:
  html_document:
    toc: true
---

# Introduction
Using the expression data from Kagale et al 2014, and the Arabidopsis to Camelina gene association list, we can look at the expression levels across multiple sample types from Camelina for a given set of genes from Arabidopsis.

## Question

 * Which genes in Camelina sativa are the best matches for the genes of interest?
 * Which of those genes are expressed in flowers and the developing seed?
 * Most importantly, which of them are expressed ONLY in flowers and/or the seed?

# Bring data together

## Create Cs gene list
Get Camelina genes of interest.

At genes of interest.
```{r}
suppressWarnings(suppressPackageStartupMessages(library(xlsx)))
fname = "data/CameliaGenesOfInterest_Qi_2.xlsx"
genesOfInt = read.xlsx(fname, startRow=1, 1)[1:3]
# only use relevant columns
genesOfInt = genesOfInt[,c("Arabidopsis.gene", "Name", "effect.summary")]
names(genesOfInt) = c("AGI", "AtSymbol", "effect.summary")
# remove any white space in ids or symbols
genesOfInt$AGI = gsub(" ", "", genesOfInt$AGI)
genesOfInt$AtSymbol = gsub(" ", "", genesOfInt$AtSymbol)
# make all ids all caps
genesOfInt$AGI = gsub("a", "A", genesOfInt$AGI)
genesOfInt$AGI = gsub("t", "T", genesOfInt$AGI)
genesOfInt$AGI = gsub("g", "G", genesOfInt$AGI)
# remove duplicate entries for the same geneID
genesOfInt = genesOfInt[!duplicated(genesOfInt$AGI),]
row.names(genesOfInt)=genesOfInt$AGI
```
This list includes `r length(unique(genesOfInt$AGI))` unique genes of interest.

Connection between At genes and Cs genes.
```{r}
source('../src/ProjectWide.R')
AtCs = getCamelinaToArabidopsisTable()
```
This list has `r nrow(AtCs)` rows. It includes `r length(unique(AtCs$AGI))` unique Arabidopsis genes and `r length(unique(AtCs$CGI))` unique Camelina genes.

Merge the lists to get a set of Camelina genes of interest.
```{r}
CsGenesOfInt = merge(AtCs, genesOfInt, by="AGI")
CsGenesOfInt$CGI = gsub(".1", "", CsGenesOfInt$CGI, fixed=T)
```

 * `r length(unique(CsGenesOfInt$AGI))` Arabidopsis genes from the original list were linked to a Camelina gene.
 * `r length(unique(CsGenesOfInt$CGI))` Camelina genes are now putative genes of interest.

Which genes of interest did not translate into Camelina genes? We may follow up with blast results later.
```{r}
orphans = setdiff(genesOfInt$AGI, CsGenesOfInt$AGI)
orphans = genesOfInt[orphans,]
```
```{r echo=FALSE}
suppressWarnings(suppressPackageStartupMessages(library(knitr)))
kable(x=orphans, digits=1, row.names = F)
```

`r nrow(orphans)` of the Arabidopsis genes of interest did not translate into any Camelina genes.

## Expression levels
Link genes of interest to expression levels in various tissues.

Read expression values.
```{r}
sample_types=getAllSampleTypes()
# counts = getRpkmCounts()
# counts = add.averages(counts)
counts.sm = getRpkmCounts("sm")
counts.sm = add.averages(counts.sm, sample_types)
counts.mm = getRpkmCounts("mm")
counts.mm = add.averages(counts.mm, sample_types)
counts.pm = getRpkmCounts("pm")
counts.pm = add.averages(counts.pm, sample_types)
```
```{r echo=FALSE}
upper.sm=sapply(counts.sm[grep('.ave', names(counts.sm))], quantile, .99)
upper.sm=round(upper.sm, digits=0)
upper.mm=sapply(counts.mm[grep('.ave', names(counts.mm))], quantile, .99)
upper.mm=round(upper.mm, digits=0)
upper.pm=sapply(counts.pm[grep('.ave', names(counts.pm))], quantile, .99)
upper.pm=round(upper.pm, digits=0)
upper=rbind(upper.sm, upper.pm, upper.mm)
upper
```
The 99% quantile is shown for reference.





Merge genes of interest list to expression values. Done't worry about keeping individual sample values, but keep the average and the standard deviation for each sample type.
```{r}
data.sm = merge(CsGenesOfInt, counts.sm[grep("ave|sd", names(counts.sm))], 
             by.x="CGI", by.y="row.names", all.x=T)
data.pm = merge(CsGenesOfInt, counts.pm[grep("ave|sd", names(counts.pm))], 
             by.x="CGI", by.y="row.names", all.x=T)
data.mm = merge(CsGenesOfInt, counts.mm[grep("ave|sd", names(counts.mm))], 
             by.x="CGI", by.y="row.names", all.x=T)
dataAves=grep("ave", names(data.sm))
data.sm[dataAves] = round(data.sm[dataAves], digits=2)
data.pm[dataAves] = round(data.pm[dataAves], digits=2)
data.mm[dataAves] = round(data.mm[dataAves], digits=2)
```
`r sum((data.mm$ESD.ave+data.mm$EMD.ave+data.mm$LMD.ave+data.mm$LSD.ave)==0)` of the `r length(unique(CsGenesOfInt$CGI))` Camelina putative genes of interest, had no expression in any stage of seed development.


```{r echo=F}
# #We are most interested in genes whose highest expression levels occur in the seeds. Ideally, genes which are expressed ONLY in the seeds.  Which genes have the best seed-expression to non-seed expression ratio? Generate a ratio by taking the highest mean expression from the four seed stages and comparing it to the highest mean expression from all of the other stages.
# highestInSeed = apply(data.sm[grep("D.ave", names(data.sm))], 1, max)
# highestNonSeed = apply(data.sm[grep("[^D].ave", names(data.sm))], 1, max)
# ratio=highestInSeed/highestNonSeed
# data.sm$SeedRatio=ratio
# names(ratio)=data.sm$CGI
# ratio = ratio[order(ratio, decreasing = T)]
```

```{r echo=FALSE}
# # For some genes, this ratio is greater than 10.
# show=data.frame(CGI=names(ratio), ratio=ratio)
# show=merge(data.sm[c("CGI", "AGI", "AtSymbol")], show, by="CGI")
# row.names(show)=show$CGI
# show=show[names(ratio),]
# w=show$ratio>10 & !is.na(show$ratio>10)
# kable(x=show[w,], digits=1, row.names = F)
#The ratio was only calculated using sm. the pm and mm ratios are probably very similar, and it is convenient to keep these data frames in exactly the same order
```

Put the genes in order by expression levels in the flower (from highest to lowest).
```{r}
ord=order(data.sm$Flw.ave, decreasing=T)
row.names(data.sm) = data.sm$CGI
data.sm = data.sm[ord,]

row.names(data.pm) = data.pm$CGI
data.pm = data.pm[ord,]

row.names(data.mm) = data.mm$CGI
data.mm = data.mm[ord,]
```

## Export Cs gene list with values.
```{r}
fname = "results/GenesOfInterest-02-allSamples-sm.txt"
write.table(data.sm, fname, sep='\t', col.names = T, row.names=F, quote=F)
```

# Plot
Group the genes based on their Arabidopsis counterpart.
```{r}
plotCols=c(1:4,grep("ave",names(data.sm)))
dataBits.sm = split(data.sm[,plotCols], f=data.sm$AGI)
dataBits.pm = split(data.pm[,plotCols], f=data.pm$AGI)
dataBits.mm = split(data.mm[,plotCols], f=data.mm$AGI)
```

Apply the plot function to each group.
```{r fig.width=6, fig.height=5}
AtGenesToPlot = unique(data.sm$AGI)
symbols = gsub("/","or",data.sm$AtSymbol[!duplicated(data.sm$AGI)])
names(symbols) = data.sm$AGI[!duplicated(data.sm$AGI)]

categories=getAllSampleTypes()
w=!duplicated(categories)
categories=categories[w]
catNames=getAllSampleTerms()[w]

for (agi in AtGenesToPlot){
  DF=list(mm=dataBits.mm[[agi]],
          pm=dataBits.pm[[agi]],
          sm=dataBits.sm[[agi]])
  makeGeneLinePlot(DF, categories=categories, catNames=catNames,
                  fname=paste0("results/",agi,"-", symbols[agi], ".png"), dpi=600)
}
```


# Conclusion

Many of these genes were detected in buds and flowers, but none appear to be unique to buds and/or flowers, with the possible exception of Csa07g051880, which is expressed at a very low level and (almost) exclusively in buds.


***Session Info***
```{r}
sessionInfo()
```
```{r echo=F}
# getCamelinaToArabidopsisTable
# getRpkmCounts
# add.averages
# getDeGenes
# getAllSampleTypes
# getAllSampleTerms
# makeGeneLinePlot
```

