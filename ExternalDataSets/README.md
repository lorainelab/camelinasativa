ExternalDataSets Module README
###########################################

The files contained in this module were downloaded from other sources for use in the analysis. This README will provide information on where the files came from and the data they contain.

***

# aralip_data.xlsx
Excel file of Arabidopsis genes involved in lipid synthesis, downloaded from http://aralip.plantbiology.msu.edu/about
cite:
Li-Beisson, Y., Shorrosh, B., Beisson, F., Andersson, M. X., Arondel, V., Bates, P. D., et al. (2010). Acyl-Lipid Metabolism. The Arabidopsis Book, 8, e0133. doi:10.1199/tab.0133.s1

***

# ATH_GO_GOSLIM.txt.gz
Arabidopsis Gene Ontology downloaded from https://www.arabidopsis.org/ by accessing "Download" then "Go and PO ontologies" 
-Contents: Arabidopsis Gene Identification and Corresponding Gene Ontology information. 

***

# C_sativa_DH55_Apr_2014.bed.gz
Bed file created from GFF annotation file Cs_genes_v2_annot.gff3.gz dowloaded from Genome Praire Camelina database Web site. 

Converted from GFF to bed using gff3ToBedDetail.py in https://bitbucket.org/lorainelab/genomes_src.

***
# codeBook.xlsx

Contains SRA accession numbers, sample names and descriptions.

***
# Csativa_Atha_sytelogMatrix.xlsx

Supplementary date file from "The emerging biofuel crop Camelina sativa retains a highly undifferentiated hexaploid genome structure"
PDF of paper is available in SupFiles folder.
Contents: Syntelog matrix representing individual Arabidopsis thaliana genes and the corresponding triplets of Camelina sativa homeologues 

***

# Csativa_transcriptome sequencing_SRA codes.xlsx

List of SRA accession codes that correspond to RNA-seq data uploaded from above mentioned publication.
This is how we determined which SRA files to use for the stages we were interested in.
Spreadsheet was sent to us from the corresponding author listed. 

***
# CytokininRelatedGenes.csv
List of Arabisposis genes related to cytokinin. Uploaded by Ivory to Base Camp. 
List is this list includes genes from all stages of the cytokinin response pathway, not just downstream genes which show a change in response to cytokinin application.
Gene Family column helps to show how the gene is related to cytokinin.

***
# gene_sizes.csv
List of the size of each gene removed from the included BED file. Removed columns using BASH commands to not have to import the whole BED file into an R script.

***
# TheGoldenList.csv
The golden list is taken from this publication:
Bhargava, A., Clabaugh, I., To, J. P., Maxwell, B. B., Chiang, Y. H., Schaller, G. E., et al. (2013). Identification of Cytokinin-Responsive Genes Using Microarray Meta-Analysis and RNA-Seq in Arabidopsis. Plant Physiology, 162(1), 272–294. doi:10.1104/pp.113.217026
This paper was a meta-analysis between 13 cytokinin micro-array datasets and 1 RNA-Seq data set.  The list represents genes that are most commonly found to be up-regulated in response to cytokinin treatment, even across several different conditions and means of treatment. 

***
# License

Copyright (c) 2015 University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT
