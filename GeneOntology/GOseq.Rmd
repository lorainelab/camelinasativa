Gene Ontology Enrichment Analysis of Differentially Expressed Genes
========================================================

Introduction
------------

Differential expression (DE) nalysis identifed many genes that are DE between stages. The later stages had more DE genes. 

Using Gene Ontology enrichment analysis, we will identify which functions and processes many DE genes belong to.

Questions:

* Which comparisons ahve the most categories?
* Which genes have highest expression?
* What are the expression levels at each stage for genes identified to play a role in cytokinin?

Analysis
--------

### GO terms

Using suplimentary data from Kagale, et al we have a list of syntelogs with the Arabidopsis gene identifer (AGI) and corresponding Camelina Sativa gene identifier (CGI). The Camelina genome contains three sub-genomes. Some AGI will have multiple Camelina genes associated.

Create GO annotation file for Camelina Sativa:

-Rows that do not contain CGI for a AGI have been removed prior to use here.

```{r}
#list of camelina-arabisopsis genes
require(openxlsx)
CandA='../ExternalDataSets/Csativa_Atha_sytelogMatrix.xlsx'
CandA=read.xlsx(CandA, startRow = 3)
#Remove period from col names
names(CandA)=gsub(".", "_", names(CandA), fixed= T)
names(CandA)=c("Block","AGI","Cs_G1","Cs_G2","Cs_G3")
#Use AGI as row names
row.names(CandA)=CandA$AGI

#Table that contains AGI, GO ID, definition, and tree
AGI='../ExternalDataSets/ATH_GO_GOSLIM.txt.gz'
AGI=read.delim(AGI, header=F, sep="\t")
AGI=AGI[c(1,4,5,6,8,9)]
names(AGI)=c("AGI","relationship","deff","GO_ID","tree","process")
```

Create table that contains AGI, CGI, GO_ID, def, and tree. Write to file.

```{r}
merged_table=merge(CandA,AGI)
#Write to file
write.table(merged_table, file="results/AGI_CGI_merged.csv", sep=",",row.names=F)
rm(CandA,AGI)
```

Create file of Camelina genes related to Cytokinin from list of Arabidopsis genes related to Cytokinin.

```{r}
cytokinin='../ExternalDataSets/CytokininRelatedGenes.csv'
cytokinin=read.delim(cytokinin, header=T, sep=',')
Camelina_cyto=merge(cytokinin, merged_table)
Camelina_cyto=subset(Camelina_cyto, select=c('AGI', 'Gene_Family', 'Symbol', 'Cs_G1', 'Cs_G2', 'Cs_G3'))

#get subsets, put all camelina genes in list instead of 3 columns
one=subset(Camelina_cyto, select=c('AGI', 'Gene_Family', 'Symbol', 'Cs_G1'))
two=subset(Camelina_cyto, select=c('AGI', 'Gene_Family', 'Symbol', 'Cs_G2'))
three=subset(Camelina_cyto, select=c('AGI', 'Gene_Family', 'Symbol', 'Cs_G3'))

# Change col names
names(one)=c('AGI', 'Gene_Family', 'Symbol', 'CGI')
names(two)=c('AGI', 'Gene_Family', 'Symbol', 'CGI')
names(three)=c('AGI', 'Gene_Family', 'Symbol', 'CGI')

Camelina_cyto=rbind(one,two,three)
Camelina_cyto=subset(Camelina_cyto, !duplicated(CGI))
write.table(Camelina_cyto, file="results/Camelina_cytokininList.csv", sep=",",row.names=F)
rm(one,two,three)
```


Create GO file using merged_table.

```{r}
#get subsets of merged_table
Cs_G1=subset(merged_table, select=c(Cs_G1,GO_ID,deff,tree))
Cs_G2=subset(merged_table, select=c(Cs_G2,GO_ID,deff,tree))
Cs_G3=subset(merged_table, select=c(Cs_G3,GO_ID,deff,tree))
rm(merged_table)

#change Col names
names(Cs_G1)=c("gene","GO_ID","deff","tree")
names(Cs_G2)=c("gene","GO_ID","deff","tree")
names(Cs_G3)=c("gene","GO_ID","deff","tree")

go_anno=rbind(Cs_G1,Cs_G2,Cs_G3)

rm(Cs_G1,Cs_G2,Cs_G3)

#Save in file
write.table(go_anno, file="results/Cs_GO.csv", sep=",",row.names=F)
```

GO annotations:

```{r}
gene2go=subset(go_anno, select=c("gene","GO_ID"))
rm(go_anno)
```

### Transcript sizes

Create file with transcript sizes.

```{r}
featureCountsFile='../GeneRegions/results/Camelina_Sativa_SAF_for_featureCount.tsv'
featureCounts=read.delim(featureCountsFile, header=F, sep="\t")
names(featureCounts)=c("gene","chr","start","stop","strand")
transcriptSize=subset(featureCounts, select=c("gene","start","stop"))
transcriptSize$length=(transcriptSize$stop-transcriptSize$start)
sizes=subset(transcriptSize, select=c(gene,length))

#Save to file
write.table(sizes, file="../ExternalDataSets/gene_sizes.csv", sep=",",row.names=F)

#Use gene as col names
sizes$kb=sizes$length/1000
gene_names=sizes$gene
sizes=sizes$kb
names(sizes)=gene_names
rm(gene_names, featureCounts, transcriptSize)
```

Pick FDR cutoffs for differential expression and GO term significance:

```{r}
de_FDR=0.001
go_FDR=0.01
```

Load GOSeq library:

```{r}
suppressPackageStartupMessages(library(goseq))
```

Define a function for the GO term analysis:

```{r warning=FALSE}
do.go=function(de_results=NULL,sizes=NULL,
               de_FDR=0.001,go_FDR=0.01,
               gene2go=NULL,go_defs=NULL,
               early=NULL,late=NULL){
  gene.vector=rep(0,nrow(de_results))
  de=which(de_results$padj<=de_FDR)
  gene.vector[de]=1
  names(gene.vector)=de_results$gene
  pwf<-nullp(gene.vector,plot.fit=F,bias.data=sizes[names(gene.vector)])
  GO=goseq(pwf,gene2cat=gene2go,method="Wallenius",use_genes_without_cat=TRUE)
  names(GO)[2]='FDR.over_represented'
  names(GO)[3]='FDR.under_represented'
  GO$o=p.adjust(GO$FDR.over_represented,method='BH')
  GO$u=p.adjust(GO$FDR.under_represented,method='BH')
  o=order(GO$o,decreasing=F)
  GO=GO[o,]
  v=union(which(GO$FDR.over_represented<=go_FDR),which(GO$FDR.under_represented<=go_FDR))
  GO=GO[v,]
  GO$early=rep(early,nrow(GO))
  GO$late=rep(late,nrow(GO))
  GO=GO[,c('early','late','category','ontology','term','FDR.over_represented','FDR.under_represented','numDEInCat','numInCat')]
  GO
}
```

### GO term enrichment analysis

Get enriched GO categories for all the comparisons:

```{r}
fnames=dir('../DiffExpAnalysis/results',pattern="V")
all=NULL
for (fname in fnames) {
  samples=strsplit(strsplit(fname,'\\.')[[1]][1],'V')[[1]]
  early=samples[1]
  late=samples[2]
  de_results=read.delim(file.path('../DiffExpAnalysis/results',fname),
                        sep='\t',header=T,quote="")
  row.names(de_results)=de_results$gene
  GO=do.go(de_results=de_results,
           sizes=sizes,
           de_FDR=de_FDR,go_FDR=go_FDR,
           gene2go=gene2go,
           early=early,late=late)
  all=rbind(all,GO)
}
#remove terms GOSeq didn't recognize
all=all[!is.na(all$term),]
```

Write results:

```{r}
fname='results/GO.tsv'
write.table(all,file=fname,sep='\t',row.names=F)
pth='results/'
fname=file.path(pth,'GO-DE.tsv')
write.table(all,file=fname,sep='\t',row.names=F)
system(paste('gzip -f',fname))
```

How many categories were over-represented in each comparison?

```{r}
comp=paste0(all$early,'-',all$late)
v=which(all$FDR.over_represented<=go_FDR)
comp=comp[v]
table(comp)
```

How many categories were enriched in adjacent comparisons?

```{r}
table(comp)[c('ESD-EMD','EMD-LMD','LMD-LSD')]
```

### Compare ESD to EMD

```{r}
comps=which(all$early=='ESD'&all$late=='EMD')
gvp=all[comps,]
o=order(gvp$FDR.over_represented)
gvp=gvp[o,]
gvp=gvp[,3:9]
fname='results/G-ESD-EMD.tsv'
write.table(gvp,file=fname,row.names=F,sep='\t',quote=F)
```

Terms significantly over-represented between ESD and EMD include: 

```{r}
unique(gvp[gvp$FDR.over_represented<=go_FDR,]$term)
```

Terms significantly under-represented between ESD and EMD stages include: 

```{r}
unique(gvp[gvp$FDR.under_represented<=go_FDR,]$term)
```

### Cytokinin specific ESD to EMD:

```{r}
names(gene2go)=c('gene','category')
de=read.delim('../DiffExpAnalysis/results/ESDVEMD.tsv.gz',sep='\t',header=T,quote="")
de=de[de$padj<=de_FDR,]
GOplusGenes=merge(gvp,gene2go[gene2go$gene%in%de$gene,],
                 by.x='category',by.y='category')
GOplusGenes=merge(GOplusGenes,
                  de[,c('gene','logFC','descr')],
                  by.x='gene',by.y='gene')
GOplusGenes=GOplusGenes[,c('category','term',
                           'numDEInCat','numInCat',
                           'FDR.over_represented',
                           'FDR.under_represented',
                           'gene','logFC','descr')]
o=order(GOplusGenes$FDR.over_represented)
GOplusGenes=GOplusGenes[o,]
cyto_genes=subset(GOplusGenes,Camelina_cyto$CGI%in%GOplusGenes$gene)
```

Of `r cyto_genes$numInCat[1]` cytokinin related genes `r cyto_genes$numDEInCat[1]` were DE between ESD and EMD stages. Of the DE genes, `r sum(cyto_genes$logFC<0)` were down-regulated and `r sum(cyto_genes$logFC>0)` were up-regulated.

Session information
==================

```{r}
sessionInfo()
```

