---
title: "Comparing young and old leaves"
author: "Ann Loraine"
date: "November 1, 2016"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

Which genes are differentially expressed (DE) between young versus mature seeds?

Use EdgeR to identify DE genes. 

## Analysis

Load counts data:

```{r}
countStyle="sm"
fname=paste0("../Counts/data/counts_", countStyle, ".tsv.gz")
fc=read.delim(fname,header=T, comment.char="#") # fc for feature counts
names(fc) = gsub(".bam", "", names(fc))
row.names(fc)=fc$Geneid
counts=fc[grep("SRR", names(fc))]
```

Fix sample names:

```{r}
fname="../ExternalDataSets/codeBook.txt"
cb=read.delim(fname, header=T, as.is=T)
sampleNames=cb$Sample.Name
names(sampleNames)=cb$SRA.Run.Accession
names(counts) = sampleNames[names(counts)]
counts=counts[sampleNames]
```

Get early and late seeds expression data:

```{r}
s1="ESD"
s2="LSD"
es=grep(s1,names(counts))
ls=grep(s2,names(counts))
counts=counts[,c(es,ls)]
```

How many millions of reads were there?

```{r}
colSums(counts)/10**6
```

Get annnotations:

```{r}
fname='../ExternalDataSets/C_sativa_DH55_Apr_2014.bed.gz'
annots=read.delim(fname,header=F,sep='\t',as.is=T,quote="")
annots=annots[c(13,14)]
names(annots)=c('gene','descr')
annots=annots[!duplicated(annots$gene),]
row.names(annots)=annots$gene
```

Create objects need to run the edgeR analysis:

```{r message=FALSE,warning=FALSE}
library(edgeR)
group=c(rep(s1,3),rep(s2,3))
cds=DGEList(counts,group=group,genes=annots)
```

Remove rows with zero counts:

```{r}
zeros=apply(cds$counts,1,sum)==0
allzeros=sum(zeros)
cds=cds[!zeros,]
```

There were `r allzeros` genes with zero counts in all `r ncol(counts)` samples.

Identify differentially expressed genes:

```{r}
cds = calcNormFactors(cds)
cds = estimateCommonDisp(cds)
prior.df=50/(ncol(counts)-length(unique(group)))
cds=estimateTagwiseDisp(cds,prior.df=prior.df)
dex=exactTest(cds,dispersion="tagwise",pair=c(s1,s2))
```

Make log counts table:

```{r}
cpms=counts[row.names(dex$table),
            c(grep(s1,colnames(counts)),
            grep(s2,colnames(counts)))]
```

Write just these counts:

```{r}
fname="results/SeedComparisonCounts.txt"
to_write=data.frame(gene=row.names(cpms),cpms)
write.table(to_write,file=fname,row.names = F,
            sep="\t",quote = F)
```

Do primitive normalization and take log base 10:

```{r}
lib.sizes=colSums(cpms)
for (j in 1:ncol(cpms)) {
  cpms[,j]=cpms[,j]/lib.sizes[j]*1000000
}
log.cpms=log10(cpms)
for (j in 1:ncol(log.cpms)) {
  minimum=min(log.cpms[log.cpms[,j]>-Inf,j])
  log.cpms[log.cpms[,j]==-Inf,j]=minimum
}
```

Write results:

```{r}
o = order(dex$table$PValue,decreasing = F)
v = row.names(dex$table)[o]
res=data.frame(gene=v,
               descr=annots[v,]$descr,
               p.value=dex$table[v,]$PValue,
               log.cpms[v,])
fname="results/SeedComparison.txt"
write.table(res,file=fname,row.names=F,sep='\t',quote=F)
```

Do t test for comparison:

```{r}
res$t.pvalue=NA
for (i in 1:nrow(res)) {
  res[i,"t.pvalue"]=t.test(res[i,grep(s1,names(res))],
                           res[i,grep(s2,names(res))])$p.value
}
fname="results/SeedComparisonWithT.txt"
write.table(res,file=fname,row.names=F,sep='\t',quote=F)
```

Visualize p values:

```{r}
par(mfrow=c(1,2))
hist(res$p.value,main="P values from EdgeR")
hist(res$t.pvalue,main="P values from Welch's T-test")
```

```{r}
alpha=0.0001
```

How many genes have p values below `r alpha` from both tests?

The EdgeR comparison produced `r sum(res$p.value<=alpha)` p values less than `r alpha`.

The t-test produced `r sum(res$t.pvalue<=alpha)` p values less than `r alpha`.

There were `r sum(res$p.value<=alpha & res$t.pvalue <= alpha)` genes where each test yielded a p value less than `r alpha`.

### Testing files

Try to reload files:

```{r}
fname="results/SeedComparison.txt"
test=read.table(fname,sep='\t',as.is=T,header=T)
fname="results/SeedComparisonCounts.txt"
test=read.table(fname,sep='\t',as.is=T,header=T)
```

## Conclusion

Many genes were differentially expressed. 

The EdgeR test produced many more apparently significant results.

## Limitations of the Analysis

Coverage was low.

## Session info


```{r}
sessionInfo()
```


