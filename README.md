# Analysis of RNA-Seq data from Camelina sativa

This project investigates gene expression and alternative splicing
in the model oilseed plant Camelina sativa using RNA-Seq data from:

* Kagale et al http://www.nature.com/ncomms/2014/140423/ncomms4706/full/ncomms4706.html (open access Camelina genome paper)

To view RNA-Seq data (coverage graphs, read alignments, splice
junctions) download [Integrated Genome Browser](http://www.bioviz.org)
and select species Camelina sativa.  Data sets from Kagale et al (and
other studies) are available in the Data Access tab. See IGB
documentation for details.

*** 

# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Blakley iclabau@uncc.edu

***

# About this repository

Folders represent mostly self-contained analysis modules. Many modules 
use results from other modules. 

* * *

# What's here

## ClusterAnalysis-STEM

* Used STEM to cluster genes by expression.
* Used violin plots to visualize cluster profiles.
* Decided to drop this and use mfuzz instead.
* Depends on gene expression tables created in Counts module.

***

## ClusterAnalysis-mfuzz

* Used mfuzz from BioConductor to cluster genes by expression.
* Used utilities from mfuzz for visualization.
* Depends on gene expression tables created in Counts module.

If you want to use expression clusters for functional prediction, synthetic promoter design, etc - Ann recommends you use these results, not the STEM results. Also, vary parameters to fine-tune results for your specific application.

***

## Counts

* Used featureCounts from subread to count number of reads overlapping genes, using different parameters for dealing with multi-mapping reads.
* Created gene expression tables as scaled & unscaled FPKM.
* Depends on GeneRegions

***

## DiffExpAnalysis

* Identifies differentially expressed genes from various comparisons
* Useful for understanding transitions between stages. 

***

## ExternalDataSets

* Contains files from external Web sites.
* Contains files mapping Sequence Read Archive run identifiers onto sample types.

Mappings between SRA identifiers were obtained from Kogale et al authors. 

***

## GeneOntology

* Uses GOSeq to identify Gene Ontology categories with unusually many differetially expressed genes.
* Depends on DiffExpAnalysis

***
## Genes-of-interest-1

* Creates plots showing expression profiles for test genes. 

***

## GeneRegions

* Creates SAF file needed to run featureCounts (Counts)

***

## Quickload

* Code for building IGBQuickload repository. Obsolete. Use src/makeCamelinaAnnotsXml.py instead.

***

## SeedSizeGenes

* Expression profiles for genes likely to increase Camelina seed size when over-expressed or knocked out.
* Preliminary data used for an NC Biotechnology grant from 2015, not funded.

***

## SeedOilGenes

* Investigates expression profiles of genes involved in oil production.

***

## src

* Contains python, R, bash shell scripts

***

# License

Copyright (c) 2015 University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT