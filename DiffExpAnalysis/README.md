Use R to perform Differential Expression Analysis on Camelina Sativa RNA-seq data.
4 seed development time points are used: 

* Early seed devlopment (ESD; 4-12 DPA)
* Early-mid seed devlopment (EMSD; 18 and 22 DPA)
* Late-mid seed development (LMSD; 26 and 30 DPA)
* Late seed development (LSD; 32 and 40 DPA)

***
INSTRUCTIONS

* Run DeAllVsAll.Rmd first
* CytoAllvAll.Rmd and CytoGraph.Rmd can be run in any order after DeAllVsAll.Rmd 

***
***
# CytoAllvAll.Rmd
The goal of this analysis is to identify which genes, if any, are up or down regulated during seed development for Cytokinin related genes.
Perform the same analysis as DeAllVsAll.Rmd for only the subset of genes that are related to Cytokinin.

***
# CytoAllvAll.html
R markdown knitted into HTML format.

***
# CytoGraph.Rmd
Graphical representation of cytokinin changes
Use logFC to represent differential expression changes of Cytokinin related genes among the four stages.
Uses data in results generated from DeAllVsAll.Rmd


***
# CytoGraph.html
R markdown knitted into HTML format.

***
# DeAllVsAll.Rmd
The goal of this analysis is to identify which genes, if any, are up or down regulated during seed development.
Perform differenital expression analysis with EdgeR.
Read in counts file from featureCounts output.
Run edgeR.
Create multi-dimensional scaling to view relationships between samples and save to results/figures.
Hierachical clustering to examine similarities between samples.
Estimate taqwise dispersion to identify differentially expressed genes.
Visualize the relationship between mean expression and variance.
Build a matrix of differential expression counts comparing every sample to every other sample.

***
# DeAllVsAll.html
R markdown knitted into HTML format.

***
